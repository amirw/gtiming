#include <cuda.h>
#include <cuda_runtime.h>
#include <stdio.h>
#include <unistd.h>
#include "gtiming.cuh"
#include <pthread.h>

#ifndef CU_CHECK
#define CU_CHECK(f) do {                        \
    cudaError_t e = f;                          \
    if (e != cudaSuccess) {                     \
        printf("Cuda failure %s:%d: '%s'\n", __FILE__, __LINE__, cudaGetErrorString(e));  \
        exit(0);                                \
    }                                           \
} while (0)
#endif

__device__ int dumb_sqr(int n) {
    gtiming_record("dumb_sqr start");
    int res = 0;
    for (int i = 0; i < n; i++) {
        res += n;
    }
    gtiming_record("dump_sqr end");
    return res;
}

__global__ void kernel1(int *d, int len, volatile int *stop) {
    gtiming_register_kernel(0, "kernel 1");

    while (!*stop) {
        gtiming_record("begin loop");
        for (int j = 0; j < 10; j++) {
            for (int i = threadIdx.x; i < len; i += blockDim.x) {
                d[i] += threadIdx.x * dumb_sqr(j);
            }
            __syncthreads();
        }
        gtiming_record("after first loop");

        for (int j = 0; j < 20; j++) {
            for (int i = threadIdx.x; i < len; i += blockDim.x) {
                d[i] *= (j % 3);
            }
            __syncthreads();
        }
        gtiming_record_last("end of loop");
    }
}

__global__ void kernel2(int *d, int len, volatile int *stop) {
    gtiming_register_kernel(1, "kernel 2");

    while (!*stop) {
        gtiming_record("begin of loop");
        for (int j = 0; j < 10; j++) {
            for (int i = threadIdx.x; i < len; i += blockDim.x) {
                d[i] += threadIdx.x + j;
            }
            __syncthreads();
        }
        gtiming_record("after first loop");

        for (int j = 0; j < 20; j++) {
            for (int i = threadIdx.x; i < len; i += blockDim.x) {
                d[i] += threadIdx.x + j * j;
            }
            __syncthreads();
        }
        gtiming_record_last("end of loop");
    }
}

void *host_thread(void *arg) {
    int *stop = (int *)arg;
    double latencies[2];
    double latencies_end_to_end[2];
    printf("sleeping for 3 seconds\n");
    sleep(3);
    printf("dumping timing to csv file\n");
    gtiming_report("timing.csv", latencies, latencies_end_to_end);
    printf("done\n");
    for (int i = 0; i < 2; i++) {
        printf("kernel%d latency: %lf usec  (e2e latency %lf usec)\n", i + 1, latencies[i] * 1e+6, latencies_end_to_end[i] * 1e+6);
    }
    *stop = 1;
    return NULL;
}

int main() {
    int threads1 = 1024;
    int threads2 = 1024;
    int blocks1 = 3;
    int blocks2 = 1;
    int device = 0;
    int arr_len1 = 100;
    int arr_len2 = 10000;

    int *garr1;
    int *garr2;
    int *stop;
    volatile int *gstop;

    CU_CHECK( cudaSetDevice(device) );

    CU_CHECK( cudaMalloc(&garr1, sizeof(int) * arr_len1 * blocks1) );
    CU_CHECK( cudaMalloc(&garr2, sizeof(int) * arr_len2 * blocks2) );
    CU_CHECK( cudaMallocHost(&stop, sizeof(int)) );
    CU_CHECK( cudaHostGetDevicePointer(&gstop, stop, 0) );
    *stop = 0;

    cudaStream_t stream1;
    cudaStream_t stream2;
    CU_CHECK( cudaStreamCreate(&stream1) );
    CU_CHECK( cudaStreamCreate(&stream2) );

    int blocks[] = {blocks1, blocks2};
    gtiming_init(device, 2, blocks, 10000);

    kernel1<<<blocks1, threads1, 0, stream1>>> (garr1, arr_len1, gstop);
    kernel2<<<blocks2, threads2, 0, stream2>>> (garr2, arr_len2, gstop);
    CU_CHECK( cudaGetLastError() );

    pthread_t host_td;
    pthread_create(&host_td, NULL, host_thread, (void *)stop);
    pthread_join(host_td, NULL);

    CU_CHECK( cudaDeviceSynchronize() );
    gtiming_destroy();

    cudaFree(garr1);
    cudaFree(garr2);
    return 0;
}

