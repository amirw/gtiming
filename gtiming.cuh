#ifndef __GTIMING_CUH__
#define __GTIMING_CUH__

#include <cuda.h>
#include <cuda_runtime.h>

#ifdef ENABLE_TIMING
/* host */
#define       gtiming_init _gtiming_init
#define       gtiming_report _gtiming_report
#define       gtiming_destroy _gtiming_destroy
/* device */
#define       gtiming_record(description)      _gtiming_record(description, sizeof(description), __FILE__, sizeof(__FILE__), __LINE__, 0);
#define       gtiming_record_last(description) _gtiming_record(description, sizeof(description), __FILE__, sizeof(__FILE__), __LINE__, 1);
#define       gtiming_register_kernel          _gtiming_register_kernel

__device__    void _gtiming_register_kernel(int kernel_id, const char description[]);
__host__      void _gtiming_init(int dev_id, int kernels, int blocks[], int max_length);
__host__      void _gtiming_report(const char raw_fname[], double latencies[], double latencies_end_to_end[]);
__host__      void _gtiming_destroy();
__device__    void _gtiming_record(const char description[], int sizeof_description, const char fname[], int sizeof_fname, long line, int last);

#else
#define gtiming_init(d, k, b, m)
#define gtiming_destroy()
#define gtiming_report(bf, l, le2e)
#define gtiming_register_kernel(k, d)
#define gtiming_record(d)
#define gtiming_record_last(d)
#endif

#endif
