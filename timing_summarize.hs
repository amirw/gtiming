import Control.Monad (liftM)
import Data.List (groupBy, sortBy, concatMap, group, sort, intercalate)
import Data.Function (on)
import Data.Maybe (fromJust, isNothing)
import Data.Monoid ((<>))
import Data.Ord (comparing)

data CsvDatum = CsvDatum {csvDatumKernel      :: Integer,
                          csvDatumKernelName  :: String,
                          csvDatumGrid        :: Integer,
                          csvDatumBlock       :: Integer,
                          csvDatumFile        :: String,
                          csvDatumLine        :: Integer,
                          csvDatumDescriptiom :: String,
                          csvDatumTi          :: Float,
                          csvDatumTf          :: Float,
                          csvDatumLst         :: Bool
                          } deriving (Show)

data StageSummary = StageSummary {stageSummaryName         :: ((String, Integer), (String, Integer)),
                                  stageSummaryDesc         :: (String, String),
                                  stageSummaryMean         :: Float,
                                  stageSummaryMedian       :: Float,
                                  stageSummaryStdev        :: Float,
                                  stageSummaryAverageCount :: Float,
                                  stageSummaryTotalSamples :: Integer,
                                  stageSummaryKernel       :: Integer,
                                  stageSummaryKernelName   :: String
                                  } deriving (Show)

data StageDiff = StageDiff {stageDiffName       :: ((String, Integer), (String, Integer)),
                            stageDiffDesc       :: (String, String),
                            stageDiffDiff       :: Float,
                            stageDiffBlock      :: Integer,
                            stageDiffKernel     :: Integer,
                            stageDiffKernelName :: String
                            } deriving (Show)

instance Eq StageDiff where
        (==) = (==) `on` stageDiffName

instance Ord StageDiff where
        compare = comparing (fst . fst . stageDiffName) <>
                  comparing (snd . fst . stageDiffName) <>
                  comparing (fst . snd . stageDiffName) <>
                  comparing (snd . snd . stageDiffName)

lineToList :: String -> [String]
lineToList = map (dropWhile (== ' ')) . filter (/= ",") . groupBy ((==) `on` (== ','))

lineToCsvDatum :: String -> CsvDatum
lineToCsvDatum l = CsvDatum {csvDatumKernel      = read k,
                             csvDatumKernelName  = kname,
                             csvDatumGrid        = read g,
                             csvDatumBlock       = read b,
                             csvDatumFile        = f,
                             csvDatumLine        = read ln,
                             csvDatumDescriptiom = desc,
                             csvDatumTi          = read ti,
                             csvDatumTf          = read tf,
                             csvDatumLst         = read lst == 1}
                where [k, g, kname, b, f, ln, desc, ti, tf, lst] = lineToList $ l

groupCsvData :: [CsvDatum] -> [[CsvDatum]]
groupCsvData d = let
                   interspered = foldr (\x acc -> if (not . csvDatumLst $ x) then (Just x):acc else (Just x):Nothing:acc) [] $ d
                   grouped = filter (and . map (not . isNothing)) . groupBy ((==) `on` isNothing) $ interspered
                   complete = filter (csvDatumLst . last) . map (map fromJust) $ grouped
                 in complete

stageDiffs :: CsvDatum -> CsvDatum -> StageDiff
stageDiffs s1 s2 =
        StageDiff {stageDiffName       = ((csvDatumFile s1, csvDatumLine s1), (csvDatumFile s2, csvDatumLine s2)),
                   stageDiffDesc       = (csvDatumDescriptiom s1, csvDatumDescriptiom s2),
                   stageDiffDiff       = csvDatumTi s2 - csvDatumTf s1,
                   stageDiffBlock      = csvDatumBlock s1,
                   stageDiffKernel     = csvDatumKernel s1,
                   stageDiffKernelName = csvDatumKernelName s1}

mean :: [Float] -> Float
mean d = sum d / (fromIntegral . length $ d)

median :: [Float] -> Float
median d
        | odd . length $ d = sorted !! mid
        | otherwise        = mean [sorted !! (mid - 1), sorted !! mid]
        where
                sorted = sort d
                mid = length d `div` 2

stdev :: [Float] -> Float
stdev d = sqrt . mean . map (\x -> (x - m) ^ 2) $ d
        where m = mean d

summarizeStage :: Integer -> [StageDiff] -> StageSummary
summarizeStage ns d = let
                                diffs = map stageDiffDiff d
                      in
                                StageSummary {stageSummaryName         = stageDiffName . head $ d,
                                              stageSummaryDesc         = stageDiffDesc . head $ d,
                                              stageSummaryMean         = mean diffs,
                                              stageSummaryMedian       = median diffs,
                                              stageSummaryStdev        = stdev diffs,
                                              stageSummaryAverageCount = (fromIntegral . length $ d) / (fromIntegral ns),
                                              stageSummaryTotalSamples = fromIntegral . length $ d,
                                              stageSummaryKernel       = stageDiffKernel . head $ d,
                                              stageSummaryKernelName   = stageDiffKernelName . head $ d}

summarizeSegments :: [[CsvDatum]] -> ([StageSummary], Float)
summarizeSegments d = let
                        diffsPerSegment = map (\seg -> zipWith stageDiffs seg (tail seg)) d
                        averageRoundTime = mean . map (sum . map stageDiffDiff) $ diffsPerSegment
                        diffs = concat diffsPerSegment
                        diffsGroupedIntoStages = group . sort $ diffs
                        totalSegments = fromIntegral . length $ d
                        stageSummaries = map (summarizeStage totalSegments) $ diffsGroupedIntoStages
                      in (stageSummaries, averageRoundTime)


summarizeKernel :: [CsvDatum] -> ([StageSummary], Float)
summarizeKernel d = let
                        blockData = groupBy ((==) `on` csvDatumBlock) d
                        segments = concatMap groupCsvData blockData
                    in summarizeSegments segments

fileToCsvData :: String -> IO [CsvDatum]
fileToCsvData f = do
        _: ll <- liftM lines . readFile $ f
        return (map lineToCsvDatum ll)

writeStageSummary :: FilePath -> FilePath -> Float -> StageSummary -> IO ()
writeStageSummary fpath fpathChart tot s = do
        let items = [show $ stageSummaryKernel s,
                     stageSummaryKernelName s,
                     (fst . stageSummaryDesc $ s) ++ " (" ++ (fst . fst . stageSummaryName $ s) ++ ":" ++ (show . snd . fst . stageSummaryName $ s) ++ ")",
                     (snd . stageSummaryDesc $ s) ++ " (" ++ (fst . snd . stageSummaryName $ s) ++ ":" ++ (show . snd . snd . stageSummaryName $ s) ++ ")",
                     show $ stageSummaryMean   s * stageSummaryAverageCount s * 10^6,
                     show $ stageSummaryMedian s * stageSummaryAverageCount s * 10^6,
                     show $ stageSummaryStdev  s * stageSummaryAverageCount s * 10^6,
                     show $ stageSummaryAverageCount s,
                     show $ stageSummaryTotalSamples s,
                     show (stageSummaryMean s * stageSummaryAverageCount s / tot * 100),
                     show $ tot * 10 ^ 6]
            stageName = (fst . stageSummaryDesc $ s) ++ " -> " ++ (snd . stageSummaryDesc $ s)
            stageTime = show $ stageSummaryMean s * stageSummaryAverageCount s * 10^6
        appendFile fpath $ (intercalate ", " items) ++ "\n"
        appendFile fpathChart $ (intercalate ", " [show $ stageSummaryKernel s,
                                                   stageSummaryKernelName s,
                                                   stageName ++ " [x " ++ (show $ stageSummaryAverageCount s) ++ "]",
                                                   stageTime,
                                                   show $ tot * 10^6]) ++ "\n"

writeSummary :: FilePath -> FilePath -> ([StageSummary], Float) -> IO ()
writeSummary fpath fpathChart (s, avg) = do
        mapM_ (writeStageSummary fpath fpathChart avg) s
        return ()

main :: IO ()
main = do
            csvData <- fileToCsvData "timing.csv"
            let kernelData = groupBy ((==) `on` csvDatumKernel) csvData
                summary = map summarizeKernel kernelData
                ofpath = "timing_summary.csv"
                ofpathChart = "chart_data.csv"
            writeFile ofpath "kernel, kernel description, from, to, mean x hits [usec], median x hits [usec], stdev x hits [usec], hits/round, total samples, % round, round [usec]\n"
            writeFile ofpathChart ""
            mapM_ (writeSummary ofpath ofpathChart) summary
            return ()
