{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}

import Text.Hamlet (shamletFile)
import Text.Blaze.Html.Renderer.String (renderHtml)
import Text.Blaze (preEscapedToMarkup)
import System.IO
import Control.Monad (liftM)
import Data.List (groupBy, intercalate)
import Data.Function (on)

data ChartDatum = ChartDatum {chartDatumKernel     :: Integer,
                              chartDatumKernelName :: String,
                              chartDatumStageName  :: String,
                              chartDatumStageTime  :: Float,
                              chartDatumTotalTime  :: Float
                              } deriving (Show)

data ChartData = ChartData {chartDataKernel     :: Integer,
                            chartDataKernelName :: String,
                            chartDataTotalTime  :: Float,
                            chartDataStages     :: [ChartDatum]}

lineToList :: String -> [String]
lineToList = map (dropWhile (== ' ')) . filter (/= ",") . groupBy ((==) `on` (== ','))

lineToChartDatum :: String -> ChartDatum
lineToChartDatum l = ChartDatum {chartDatumKernel = read k,
                                 chartDatumKernelName = kn,
                                 chartDatumTotalTime = read tt,
                                 chartDatumStageName = sn,
                                 chartDatumStageTime = read t}
                where [k, kn, sn, t, tt] = lineToList l

renderTemplate :: [ChartData] -> String
renderTemplate dataPerKernel = renderHtml $ $(shamletFile "chart_template.hamlet")

reformatStageGroup :: [(Int, String, Float)] -> (Int, [(String, Float)])
reformatStageGroup hhs@((k,_,_):_) = (k, map (\(_,a,b)->(a,b)) hhs)

main :: IO ()
main = do
    chartStagesData <- liftM (map lineToChartDatum . lines) . readFile $ "chart_data.csv"
    let chartStagesDataGrouped = groupBy ((==) `on` chartDatumKernel) chartStagesData
        chartData = map (\d -> ChartData {chartDataKernel     = chartDatumKernel . head $ d,
                                          chartDataKernelName = chartDatumKernelName . head $ d,
                                          chartDataTotalTime  = chartDatumTotalTime  . head $ d,
                                          chartDataStages     = d}) chartStagesDataGrouped
    writeFile "charts.html" $ renderTemplate chartData
    putStrLn $ "charts created in charts.html"
