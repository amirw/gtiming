CUDA_DIR=/usr/local/cuda
LIBCUDA=-L$(CUDA_DIR)/lib64 -lcudart -lcudadevrt -lnvToolsExt
CUDAINC=-I$(CUDA_DIR)/include -I$(CUDA_DIR)/samples/common/inc/
NVCC=$(CUDA_DIR)/bin/nvcc -Xptxas="-v" --generate-code code=sm_35,arch=compute_35 -maxrregcount=32 -rdc=true $(MORE_CU_CFLAGS) -O3 -DENABLE_TIMING
LIBS = -lpthread

#all: example timing_summarize chart_gen
all: example timing_summarize

example: example.o gtiming.o
	$(NVCC) -o $@ $^ $(LIBS) $(LIBCUDA)

example.o: example.cu
	$(NVCC) $(CUDAINC) -c -o $@ $<

gtiming.o: gtiming.cu gtiming.cuh
	$(NVCC) $(CUDAINC) -c -o $@ $<

timing_summarize: timing_summarize.hs
	ghc -o timing_summarize -outputdir hsoutput -O2 -rtsopts timing_summarize.hs

chart_gen: chart_gen.hs
	ghc -o chart_gen -outputdir hsoutput -O2 chart_gen.hs

clean:
	\rm -f example chart_gen timing_summarize *.o *~
	\rm -rf hsoutput *.out
	\rm -rf *.dSYM
	\rm -f *.csv
	\rm -f *.html

