#include "gtiming.cuh"
#include <stdio.h>
#include <math.h>

#define SQR(__x) ((__x) * (__x))

#define GRID_ID() ({\
                    long _gridid;\
                    asm volatile ("mov.u64 %0, %%gridid;" : "=l"(_gridid) :);\
                     _gridid;\
                   })

#ifndef CU_CHECK
#define CU_CHECK(f) do {                        \
    cudaError_t e = f;                          \
    if (e != cudaSuccess) {                     \
        printf("Cuda failure %s:%d: '%s'\n", __FILE__, __LINE__, cudaGetErrorString(e));  \
        exit(0);                                \
    }                                           \
} while (0)
#endif

#define MAX_FNAME_LEN 40
#define MAX_DESCRIPTION_LEN 40
#define MAX_KERNEL_NAME_LEN 40

struct gtiming_time_t {
    double ti;
    double tf;
    char fname[MAX_FNAME_LEN + 1];
    long line;
    char description[MAX_DESCRIPTION_LEN + 1];
    long gridid;
    int last;
    int valid;
};

struct gtiming_state_t {
    int next_idx;
};

struct gtiming_meta_t {
    int max_length;
    int kernels;
    int *blocks;
    double dev_freq;
};

           volatile struct gtiming_time_t ***gtiming_times;
__device__          struct gtiming_time_t ***gtiming_times_devptrs;
__device__          struct gtiming_state_t **gtiming_states;
                    struct gtiming_meta_t meta_h;
__device__          struct gtiming_meta_t meta_d;
__device__          int *kernelid_to_gridid;
           volatile char **kernelid_to_name;
__device__          char **kernelid_to_name_devptrs;

__host__ void _gtiming_init(int dev_id, int kernels, int blocks[], int max_length) {
    struct cudaDeviceProp prop;
    CU_CHECK ( cudaGetDeviceProperties(&prop, dev_id) );

    meta_h.max_length = max_length;
    meta_h.kernels = kernels;
    meta_h.blocks = (int *) malloc(sizeof(int) * kernels);
    for (int i = 0; i < kernels; i++)
        meta_h.blocks[i] = blocks[i];
    meta_h.dev_freq = prop.clockRate * 1e+3;

    struct gtiming_meta_t meta_d_h;
    memcpy(&meta_d_h, &meta_h, sizeof(struct gtiming_meta_t));
    CU_CHECK( cudaMalloc(&meta_d_h.blocks, sizeof(int) * kernels) );
    CU_CHECK( cudaMemcpyToSymbol(meta_d, &meta_d_h, sizeof(struct gtiming_meta_t), 0, cudaMemcpyHostToDevice) );

    struct gtiming_state_t **states_d_h;
    struct gtiming_state_t **states_d;
    states_d_h = (struct gtiming_state_t **) malloc(sizeof(struct gtiming_state_t *) * kernels);
    for (int i = 0; i < kernels; i++) {
        CU_CHECK( cudaMalloc(&states_d_h[i], sizeof(struct gtiming_state_t) * blocks[i]) );
        CU_CHECK( cudaMemset(states_d_h[i], 0, sizeof(struct gtiming_state_t) * blocks[i]) );
    }
    CU_CHECK( cudaMalloc(&states_d, sizeof(struct gtiming_state_t *) * kernels) );
    CU_CHECK( cudaMemcpy(states_d, states_d_h, sizeof(struct gtiming_state_t *) * kernels, cudaMemcpyHostToDevice) );
    CU_CHECK( cudaMemcpyToSymbol(gtiming_states, &states_d, sizeof(struct gtiming_state_t **), 0, cudaMemcpyHostToDevice) );
    free(states_d_h);

    int *kernelid_to_gridid_d;
    int *kernelid_to_gridid_h = (int*) malloc(sizeof(int) * kernels);
    for (int i = 0; i < kernels; i++)
        kernelid_to_gridid_h[i] = -1;
    CU_CHECK( cudaMalloc(&kernelid_to_gridid_d, sizeof(int) * kernels) );
    CU_CHECK( cudaMemcpy(kernelid_to_gridid_d, kernelid_to_gridid_h, sizeof(int) * kernels, cudaMemcpyHostToDevice) );
    CU_CHECK( cudaMemcpyToSymbol(kernelid_to_gridid, &kernelid_to_gridid_d, sizeof(int *), 0, cudaMemcpyHostToDevice) );
    free(kernelid_to_gridid_h);

    kernelid_to_name = (volatile char **) malloc(sizeof(char *) * kernels);
    for (int i = 0; i < kernels; i++) {
        CU_CHECK( cudaMallocHost(&kernelid_to_name[i], MAX_KERNEL_NAME_LEN + 1) );
        memset((void *)kernelid_to_name[i], 0, MAX_KERNEL_NAME_LEN + 1);
    }

    char **kernelid_to_name_devptrs_h = (char **) malloc(sizeof(char *) * kernels);
    for (int i = 0; i < kernels; i++) {
        CU_CHECK( cudaHostGetDevicePointer(&kernelid_to_name_devptrs_h[i], (void *)kernelid_to_name[i], 0) );
    }
    char **kernelid_to_name_devptrs_d;
    CU_CHECK( cudaMalloc(&kernelid_to_name_devptrs_d, sizeof(char *) * kernels) );
    CU_CHECK( cudaMemcpy(kernelid_to_name_devptrs_d, kernelid_to_name_devptrs_h, sizeof(char *) * kernels, cudaMemcpyHostToDevice) );
    CU_CHECK( cudaMemcpyToSymbol(kernelid_to_name_devptrs, &kernelid_to_name_devptrs_d, sizeof(char **), 0, cudaMemcpyHostToDevice) );
    free(kernelid_to_name_devptrs_h);

    gtiming_times = (volatile struct gtiming_time_t ***) malloc(sizeof(volatile struct gtiming_time_t **) * kernels);
    for (int i = 0; i < kernels; i++) {
        gtiming_times[i] = (volatile struct gtiming_time_t **) malloc(sizeof(volatile struct gtiming_time_t *) * blocks[i]);
        for (int j = 0; j < blocks[i]; j++) {
            CU_CHECK( cudaMallocHost(&gtiming_times[i][j], sizeof(struct gtiming_time_t) * max_length) );
            memset((void *)gtiming_times[i][j], 0, sizeof(struct gtiming_time_t) * max_length);
        }
    }

    struct gtiming_time_t ***gtiming_times_devptrs_h = (struct gtiming_time_t ***) malloc(sizeof(struct gtiming_time_t **) * kernels);
    struct gtiming_time_t ***gtiming_times_devptrs_d;
    for (int i = 0; i < kernels; i++) {
        struct gtiming_time_t **gtiming_times_devptrs_hh = (struct gtiming_time_t **) malloc(sizeof(struct gtiming_time_t *) * blocks[i]);
        struct gtiming_time_t **gtiming_times_devptrs_dd;
        for (int j = 0; j < blocks[i]; j++) {
            CU_CHECK( cudaHostGetDevicePointer(&gtiming_times_devptrs_hh[j], (void *)gtiming_times[i][j], 0) );
        }
        CU_CHECK( cudaMalloc(&gtiming_times_devptrs_dd, sizeof(struct gtiming_time_t *) * blocks[i]) );
        CU_CHECK( cudaMemcpy(gtiming_times_devptrs_dd, gtiming_times_devptrs_hh, sizeof(struct gtiming_time_t *) * blocks[i], cudaMemcpyHostToDevice) );
        free(gtiming_times_devptrs_hh);
        gtiming_times_devptrs_h[i] = gtiming_times_devptrs_dd;
    }
    CU_CHECK( cudaMalloc(&gtiming_times_devptrs_d, sizeof(struct gtiming_time_t **) * kernels) );
    CU_CHECK( cudaMemcpy(gtiming_times_devptrs_d, gtiming_times_devptrs_h, sizeof(struct gtiming_time_t **) * kernels, cudaMemcpyHostToDevice) );
    CU_CHECK( cudaMemcpyToSymbol(gtiming_times_devptrs, &gtiming_times_devptrs_d, sizeof(struct gtiming_time_t ***), 0, cudaMemcpyHostToDevice) );
    free(gtiming_times_devptrs_h);
}

__host__ void _gtiming_destroy() {
    struct gtiming_time_t ***gtiming_times_devptrs_d;
    struct gtiming_time_t ***gtiming_times_devptrs_h = (struct gtiming_time_t ***) malloc(sizeof(struct gtiming_time_t **) * meta_h.kernels);
    CU_CHECK( cudaMemcpyFromSymbol(&gtiming_times_devptrs_d, gtiming_times_devptrs, sizeof(struct gtiming_time_t ***), 0, cudaMemcpyDeviceToHost) );
    CU_CHECK( cudaMemcpy(gtiming_times_devptrs_h, gtiming_times_devptrs_d, sizeof(struct gtiming_time_t **) * meta_h.kernels, cudaMemcpyDeviceToHost) );

    for (int i = 0; i < meta_h.kernels; i++) {
        cudaFree(gtiming_times_devptrs_h[i]);
    }
    cudaFree(gtiming_times_devptrs_d);

    for (int i = 0; i < meta_h.kernels; i++) {
        for (int j = 0; j < meta_h.blocks[i]; j++) {
            cudaFreeHost((void *)gtiming_times[i][j]);
        }
        free(gtiming_times[i]);
    }
    free(gtiming_times);

    char **kernelid_to_name_devptrs_d;
    CU_CHECK( cudaMemcpyFromSymbol(&kernelid_to_name_devptrs_d, kernelid_to_name_devptrs, sizeof(char **), 0, cudaMemcpyDeviceToHost) );
    cudaFree(kernelid_to_name_devptrs_d);
    for (int i = 0; i < meta_h.kernels; i++) {
        cudaFreeHost((void *)kernelid_to_name[i]);
    }
    free(kernelid_to_name);

    int *kernelid_to_gridid_d;
    CU_CHECK( cudaMemcpyFromSymbol(&kernelid_to_gridid_d, kernelid_to_gridid, sizeof(int *), 0, cudaMemcpyDeviceToHost) );
    CU_CHECK( cudaFree(kernelid_to_gridid_d) );

    struct gtiming_state_t **gtiming_states_d;
    struct gtiming_state_t **gtiming_states_d_h = (struct gtiming_state_t **) malloc(sizeof(struct gtiming_state_t *) * meta_h.kernels);
    CU_CHECK( cudaMemcpyFromSymbol(&gtiming_states_d, gtiming_states, sizeof(struct gtiming_state_t **), 0, cudaMemcpyDeviceToHost) );
    CU_CHECK( cudaMemcpy(gtiming_states_d_h, gtiming_states_d, sizeof(struct gtiming_states *) * meta_h.kernels, cudaMemcpyDeviceToHost) );
    for (int i = 0; i < meta_h.kernels; i++) {
        cudaFree(gtiming_states_d_h[i]);
    }
    cudaFree(gtiming_states_d);
    free(gtiming_states_d_h);

    struct gtiming_meta_t meta_d_tmp;
    CU_CHECK( cudaMemcpyFromSymbol(&meta_d_tmp, meta_d, sizeof(struct gtiming_meta_t), 0, cudaMemcpyDeviceToHost) );
    cudaFree(meta_d_tmp.blocks);

    free(meta_h.blocks);
}

__device__ void _gtiming_record(const char description[], int size_description, const char fname[], int size_fname, long line, int last) {
    long ti = clock64();
    int tid = threadIdx.x;
    int threads = blockDim.x;
    int gid = GRID_ID();
    int bid = blockIdx.x;
    __shared__ int kid;
    int activethreads = (threads < 32) ? threads : 32;

    if (tid > 32)
        return;

    for (int i = tid; i < meta_d.kernels; i += activethreads) {
        if (kernelid_to_gridid[i] == gid)
            kid = i;
    }

    int next = gtiming_states[kid][bid].next_idx;

    if (next >= meta_d.max_length)
        return;

    if (tid == 0) {
        gtiming_times_devptrs[kid][bid][next].line = line;
        gtiming_times_devptrs[kid][bid][next].gridid = GRID_ID();
        gtiming_times_devptrs[kid][bid][next].last = last;
        gtiming_times_devptrs[kid][bid][next].ti = (double)ti / meta_d.dev_freq;
    }

    for (int i = tid; i < MAX_FNAME_LEN; i += activethreads) {
        if (i < size_fname)
            gtiming_times_devptrs[kid][bid][next].fname[i] = fname[i];
    }
    for (int i = tid; i < MAX_DESCRIPTION_LEN; i += activethreads) {
        if (i < size_description)
            gtiming_times_devptrs[kid][bid][next].description[i] = description[i];
    }

    if (tid == 0) {
        gtiming_times_devptrs[kid][bid][next].fname[MAX_FNAME_LEN] = '\0';
        gtiming_times_devptrs[kid][bid][next].description[MAX_DESCRIPTION_LEN] = '\0';
        long tf = clock64();
        gtiming_times_devptrs[kid][bid][next].tf = (double)tf / meta_d.dev_freq;
        gtiming_states[kid][bid].next_idx = next + 1;
        __threadfence_system();
        gtiming_times_devptrs[kid][bid][next].valid = 1;
    }
}

__device__ void _gtiming_register_kernel(int kernel_id, const char description[]) {
    kernelid_to_gridid[kernel_id] = GRID_ID();
    for (int i = 0; i < MAX_KERNEL_NAME_LEN; i++) {
        kernelid_to_name_devptrs[kernel_id][i] = description[i];
        if (description[i] == '\0') break;
    }
    kernelid_to_name_devptrs[kernel_id][MAX_KERNEL_NAME_LEN] = '\0';
}

__host__ void _gtiming_summarize(double latencies[], double latencies_end_to_end[]) {
    if (latencies || latencies_end_to_end) {
        volatile struct gtiming_time_t *t;
        for (int i = 0; i < meta_h.kernels; i++) {
            double latency_sum = 0, latency_e2e_sum = 0;
            int cnt = 0;
            for (int j = 0; j < meta_h.blocks[i]; j++) {
                double t_prev = -1, t_start = -1;
                double current_iter_latency = 0;
                for (int k = 0; k < meta_h.max_length; k++) {
                    t = &gtiming_times[i][j][k];
                    if (!t->valid) break;
                    if (t_prev > 0)
                        current_iter_latency += (t->ti - t_prev);
                    if (t_start < 0)
                        t_start = t->ti;
                    t_prev = t->tf;
                    if (t->last) {
                        latency_sum += current_iter_latency;
                        latency_e2e_sum += t->tf - t_start;
                        cnt ++;
                        t_prev = -1;
                        t_start = -1;
                        current_iter_latency = 0;
                    }
                }
            }
            if (latencies)
                latencies[i] = latency_sum / cnt;
            if (latencies_end_to_end)
                latencies_end_to_end[i] = latency_e2e_sum / cnt;
        }
    }
}

__host__ void _gtiming_report(const char fname[], double latencies[], double latencies_end_to_end[]) {
    FILE *f = fopen(fname, "w");
    volatile struct gtiming_time_t *t;
    fprintf(f, "kernel, grid, kernel description, block, file, line, description, ti [sec], tf [sec], last\n");
    for (int i = 0; i < meta_h.kernels; i++) {
        for (int j = 0; j < meta_h.blocks[i]; j++) {
            for (int k = 0; k < meta_h.max_length; k++) {
                t = &gtiming_times[i][j][k];
                if (!t->valid) break;
                fprintf(f, "%d, %ld, %s, %d, %s, %ld, %s, %.10lf, %.10lf, %d\n",
                            i, t->gridid, kernelid_to_name[i], j, t->fname, t->line, t->description, t->ti, t->tf, t->last);
            }
        }
    }
    _gtiming_summarize(latencies, latencies_end_to_end);
    fclose(f);
}
